# FPU Base 8: Fast 404 from file

This module will extend the core Fast404ExceptionHtmlSubscriber class to utilize a 404.html file located at `DRUPAL_ROOT . '/404.html'`, as opposed to an HTML string in settings.php, when serving fast 404s.

## Authors

* FPU Webteam

## Acknowledgments

* FPU's friendliest web developer
